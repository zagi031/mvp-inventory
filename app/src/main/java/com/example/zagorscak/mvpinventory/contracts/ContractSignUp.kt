package com.example.zagorscak.mvpinventory.contracts

import android.net.ConnectivityManager
import com.google.firebase.auth.FirebaseUser

interface ContractSignUp {
    interface ViewContract {
        fun onConnected()
        fun onNotConnected()
        fun setInputErrors(emailError:String, passwordError:String)
        fun onCorrectInput(email: String, password: String)
        fun onSuccessfulSignUp()
        fun onUnsuccessfulSignUp()
        fun getName():String
        fun getSurname():String
    }

    interface PresenterContract {
        fun getCurrentUser(): FirebaseUser?
        fun checkConnectivity(cm: ConnectivityManager)
        fun checkInput(email: String, password: String)
        fun signUp(email:String, password:String)
    }
}