package com.example.zagorscak.mvpinventory.listenerInterfaces

interface onWarehouseClickListener {
    fun onWarehouseClick(position:Int)
    fun onWarehouseLongClick(position: Int)
}