package com.example.zagorscak.mvpinventory.views

import android.R.attr.name
import android.app.Service
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.zagorscak.mvpinventory.R
import com.example.zagorscak.mvpinventory.contracts.ContractItemFragment
import com.example.zagorscak.mvpinventory.models.Warehouse
import com.example.zagorscak.mvpinventory.presenters.ItemFragmentPresenter
import kotlinx.android.synthetic.main.fragment_item.*


class ItemFragment : Fragment(), ContractItemFragment.ViewContract {
    private lateinit var presenter: ContractItemFragment.PresenterContract
    private var changeToWarehouseFragmentWhileOnPause = false

    companion object {
        fun getInstance(warehouseID: String, itemID: String): ItemFragment {
            val itemFragment = ItemFragment()
            itemFragment.presenter = ItemFragmentPresenter(itemFragment, warehouseID, itemID)
            return itemFragment
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    override fun onDestroy() {
        presenter.setView(null)
        super.onDestroy()
    }
    private fun setListeners() {
        btn_item_quantity_minus.setOnClickListener {
            if (tv_item_quantity.text.toString().toInt() > 0) {
                var value = tv_item_quantity.text.toString().toInt()
                value--
                tv_item_quantity.text = value.toString()
            }
        }

        btn_item_quantity_plus.setOnClickListener {
            if (tv_item_quantity.text.toString().toInt() < presenter.getMaxItemQuantity()) {
                var value = tv_item_quantity.text.toString().toInt()
                value++
                tv_item_quantity.text = value.toString()
            }
        }

        btn_set_quantity.setOnClickListener {
            if (tv_item_quantity.text.toString().toInt() == 0) {
                presenter.deleteItem()
            } else if (tv_item_quantity.text.toString().toInt() != presenter.getItemQuantity()) {
                presenter.setNewQuantity(tv_item_quantity.text.toString().toInt())
            }
        }

        btn_move.setOnClickListener {
            if (spinner_pick_warehouse.selectedItem != null) {
                presenter.moveItem(
                    numPicker_quantity_to_move.value,
                    spinner_pick_warehouse.selectedItem as Warehouse
                )
            } else showToast("Invalid warehouse")
        }

        btn_rename.setOnClickListener {
            if (et_rename.text.isNotBlank()) {
                presenter.renameItem(et_rename.text.toString().trim())
//                hide focus on edit text rename
                val imm =
                    context?.getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(et_rename.windowToken, 0)
                et_rename.text.clear()
            } else showToast("New name must not be blank")
        }
    }
    override fun setUI() {
        tv_item_name.text = presenter.getItemName()
        tv_item_quantity.text = presenter.getItemQuantity().toString()
        numPicker_quantity_to_move.minValue = 1
        numPicker_quantity_to_move.maxValue = presenter.getItemQuantity() ?: 0
    }
    override fun changeToWarehousesFragment() {
        try {
            //      ERROR - when app paused and while on this fragment,
            //      and deleted that item from another device
            clearBackStack()
            fragmentManager?.beginTransaction()
                ?.replace(R.id.fragment_container, WarehousesFragment.getInstance())
                ?.commitAllowingStateLoss()
        } catch (ignored: IllegalStateException) {
            // There's no way to avoid getting this if saveInstanceState has already been called.
            changeToWarehouseFragmentWhileOnPause = true
        }
    }
    private fun clearBackStack() {
        for (i in 0..(fragmentManager?.backStackEntryCount ?: 0)) {
            fragmentManager?.popBackStack()
        }
    }
    override fun onResume() {
        super.onResume()
        if (changeToWarehouseFragmentWhileOnPause) {
            changeToWarehouseFragmentWhileOnPause = false
            changeToWarehousesFragment()
        }
    }
    override fun setSpinnerData(warehouses: List<Warehouse>) {
        val adapter = context?.let {
            ArrayAdapter<Warehouse>(
                it,
                R.layout.support_simple_spinner_dropdown_item,
                warehouses
            )
        }
        adapter?.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spinner_pick_warehouse.adapter = adapter
    }
    override fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}
