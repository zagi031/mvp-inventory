package com.example.zagorscak.mvpinventory.views

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.zagorscak.mvpinventory.R
import com.example.zagorscak.mvpinventory.contracts.ContractSignUp
import com.example.zagorscak.mvpinventory.presenters.SignUpPresenter
import kotlinx.android.synthetic.main.activity_signup.*


class SignUpActivity : AppCompatActivity(), ContractSignUp.ViewContract {
    private val presenter:ContractSignUp.PresenterContract = SignUpPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        btn_signUp_signUp.setOnClickListener {
            presenter.checkConnectivity(getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        }
    }

    override fun setInputErrors(emailError: String, passwordError: String) {
        til_signUp_email.error = emailError
        til_singUp_password.error = passwordError
    }

    override fun onConnected() {
        val email = et_signUp_email.text.toString()
        val password = et_signUp_password.text.toString()
        presenter.checkInput(email, password)
    }

    override fun onNotConnected() {
        Toast.makeText(this, "Check internet connection", Toast.LENGTH_LONG).show()
    }

    override fun onCorrectInput(email: String, password: String) {
        presenter.signUp(email, password)
    }

    override fun onSuccessfulSignUp() {
        val loggedInIntent = Intent(
            this, MainActivity::class.java
        ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(loggedInIntent)
        Toast.makeText(this, "Welcome " + (presenter.getCurrentUser()?.displayName), Toast.LENGTH_SHORT).show()
    }

    override fun onUnsuccessfulSignUp() {
        Toast.makeText(this, "You have been already registered", Toast.LENGTH_SHORT).show()
    }

    override fun getName() = et_name.text.toString()

    override fun getSurname() = et_surname.text.toString()
}
