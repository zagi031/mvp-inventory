package com.example.zagorscak.mvpinventory.models

class Item {
    var id:String = ""
    var name:String = ""
    var quantity:Int = 0
}
