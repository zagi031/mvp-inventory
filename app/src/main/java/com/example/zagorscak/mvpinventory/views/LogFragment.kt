package com.example.zagorscak.mvpinventory.views

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.DialogCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.example.zagorscak.mvpinventory.R
import com.example.zagorscak.mvpinventory.contracts.ContractLog
import com.example.zagorscak.mvpinventory.presenters.LogFragmentPresenter
import kotlinx.android.synthetic.main.fragment_log.*


class LogFragment : Fragment(), ContractLog.ViewContract {
    private val presenter:ContractLog.PresenterContract = LogFragmentPresenter(this)

    companion object {
        fun getInstance(): LogFragment {
            return LogFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_log, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkConnection()
        tv_logData.movementMethod = ScrollingMovementMethod()
        swipeToRefresh_fragment_log.setOnRefreshListener {
            fragmentManager?.beginTransaction()?.detach(this)?.attach(this)?.commit()
        }

        fbtn_deleteAllLogs.setOnClickListener {
            AlertDialog.Builder(context).setTitle("Warning")
                .setMessage("Really want to delete whole log data?")
                .setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                    presenter.deleteAllLogs()
                })
                .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                }).show()
        }
    }

    private fun checkConnection() {
        presenter.checkConnectivity(activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)

    }

    override fun onConnected(logData: String) {
        fbtn_deleteAllLogs.visibility = View.VISIBLE
        showLogData(logData)
        Toast.makeText(context, "Newest log", Toast.LENGTH_SHORT).show()
    }

    override fun onNotConnected() {
        fbtn_deleteAllLogs.visibility = View.GONE
        showLogData("Not connected to internet")
        Toast.makeText(
            context,
            "Check your internet connectivity and refresh",
            Toast.LENGTH_LONG
        ).show()
    }

    override fun showLogData(logData: String) {
        tv_logData.text = logData
    }

    override fun onDestroy() {
        presenter.setView(null)
        super.onDestroy()
    }
}
