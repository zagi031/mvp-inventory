package com.example.zagorscak.mvpinventory.contracts

interface ContractSplashScreen {
    interface ViewContract{
        fun onUserLoggedIn()
        fun onUserNotLoggedIn()
    }

    interface PresenterContract{
        fun checkUserLogin()
    }
}