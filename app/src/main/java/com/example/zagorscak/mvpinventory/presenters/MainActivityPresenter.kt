package com.example.zagorscak.mvpinventory.presenters

import com.example.zagorscak.mvpinventory.contracts.ContractMainActivity
import com.google.firebase.auth.FirebaseAuth

class MainActivityPresenter(private val viewMainActivity: ContractMainActivity.ViewContract) : ContractMainActivity.PresenterContract {
    private val mAuth = FirebaseAuth.getInstance()

    override fun getCurrentUser() = mAuth.currentUser

    override fun signOut() {
        mAuth.signOut()
        if(mAuth.currentUser == null) {
            viewMainActivity.onSuccessfulSignOut()
        }
        else viewMainActivity.onUnSuccessfulSignOut()
    }
}
