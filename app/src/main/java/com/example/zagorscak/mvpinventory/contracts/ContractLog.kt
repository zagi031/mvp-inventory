package com.example.zagorscak.mvpinventory.contracts

import android.net.ConnectivityManager

interface ContractLog {
    interface ViewContract {
        fun onConnected(logData: String)
        fun onNotConnected()
        fun showLogData(logData: String)
    }

    interface PresenterContract{
        fun checkConnectivity(cm: ConnectivityManager)
        fun setView(view: ViewContract?)
        fun deleteAllLogs()
    }
}