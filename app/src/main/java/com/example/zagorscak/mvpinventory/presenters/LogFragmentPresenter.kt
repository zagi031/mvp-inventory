package com.example.zagorscak.mvpinventory.presenters

import android.net.ConnectivityManager
import com.example.zagorscak.mvpinventory.contracts.ContractLog
import com.example.zagorscak.mvpinventory.models.LogData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class LogFragmentPresenter(private var viewLog: ContractLog.ViewContract?) : ContractLog.PresenterContract {
    private val mDatabase = FirebaseDatabase.getInstance()
    private val mDbRef = mDatabase.reference.child("logs")
    private var logData = ""

    init {
        mDbRef.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                viewLog?.showLogData(p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                var result = ""
                p0.children.forEach{
                    val log = LogData()
                    log.action = it.getValue(LogData::class.java)?.action.toString()
                    log.time = it.getValue(LogData::class.java)?.time.toString()
                    log.user = it.getValue(LogData::class.java)?.user.toString()
                    result += formatLog(log)
                }
                logData = result
                viewLog?.showLogData(result)
            }
        })
    }

    override fun checkConnectivity(cm: ConnectivityManager) {
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        if(isConnected){
            viewLog?.onConnected(this.logData)
        }
        else viewLog?.onNotConnected()
    }

    private fun formatLog(log: LogData):String {
        return "\u2022 [${log.user}]@${log.time} : ${log.action}\n"
    }

    override fun setView(view: ContractLog.ViewContract?) {
        this.viewLog = view
    }

    override fun deleteAllLogs() {
        mDbRef.removeValue()
    }
}