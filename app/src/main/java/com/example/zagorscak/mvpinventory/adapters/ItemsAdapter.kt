package com.example.zagorscak.mvpinventory.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.zagorscak.mvpinventory.R
import com.example.zagorscak.mvpinventory.models.Item
import com.example.zagorscak.mvpinventory.listenerInterfaces.onItemClickListener
import kotlinx.android.synthetic.main.item.view.*

class ItemsAdapter(private val itemClickListener: onItemClickListener):RecyclerView.Adapter<ItemHolder>() {
    private val items:MutableList<Item> = mutableListOf()

    fun refreshData(items: MutableList<Item>){
        this.items.clear()
        this.items.addAll(items)
        this.notifyDataSetChanged()
    }

    fun addItem(item: Item){
        this.items.add(item)
        this.notifyItemInserted(itemCount-1)
    }

    fun deleteItem(item: Item){
        val position = this.items.indexOf(item)
        this.items.remove(item)
        this.notifyItemRemoved(position)
    }

    fun getItem(position: Int) = items[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(LayoutInflater.from(parent.context).inflate(R.layout.item,parent,false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ItemHolder, position: Int) = holder.bind(items[position], itemClickListener)
}

class ItemHolder(itemView: View):RecyclerView.ViewHolder(itemView){

    @SuppressLint("SetTextI18n")
    fun bind(item:Item, listener: onItemClickListener){
        itemView.tv_item_name.text = "Name: ${item.name}"
        itemView.tv_item_quantity.text = "Quantity: ${item.quantity}"
        itemView.setOnClickListener{listener.onItemClick(adapterPosition)}
    }
}