package com.example.zagorscak.mvpinventory.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.zagorscak.mvpinventory.R
import com.example.zagorscak.mvpinventory.models.Warehouse
import com.example.zagorscak.mvpinventory.listenerInterfaces.onWarehouseClickListener
import kotlinx.android.synthetic.main.warehouse.view.*

class WarehousesAdapter(private val warehouseClickListener: onWarehouseClickListener) :
    RecyclerView.Adapter<WarehouseHolder>() {
    private val warehouses: MutableList<Warehouse> = mutableListOf()

    fun refreshData(warehouses: MutableList<Warehouse>) {
        this.warehouses.clear()
        this.warehouses.addAll(warehouses)
        this.notifyDataSetChanged()
    }

    fun addWarehouse(warehouse: Warehouse) {
        this.warehouses.add(warehouse)
        this.notifyItemInserted(itemCount - 1)
    }

    fun deleteWarehouse(warehouse: Warehouse) {
        val position = this.warehouses.indexOf(warehouse)
        this.warehouses.remove(warehouse)
        this.notifyItemRemoved(position)
    }

    fun getWarehouse(position: Int) = warehouses[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WarehouseHolder {
        return WarehouseHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.warehouse, parent, false)
        )
    }

    override fun onBindViewHolder(holder: WarehouseHolder, position: Int) =
        holder.bind(warehouses[position], warehouseClickListener)

    override fun getItemCount() = warehouses.size
}

class WarehouseHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    @SuppressLint("SetTextI18n")
    fun bind(warehouse: Warehouse, listener: onWarehouseClickListener) {
        itemView.tv_item_warehouse_name.text = "Name: ${warehouse.name}"
        itemView.tv_item_warehouse_address.text = "Address: ${warehouse.address}"
        itemView.tv_item_warehouse_capacity.text = "Capacity: ${warehouse.getCurrentCapacity()}/${warehouse.maxCapacity}"
        itemView.setOnClickListener { listener.onWarehouseClick(adapterPosition) }
        itemView.setOnLongClickListener(object : View.OnLongClickListener {
            override fun onLongClick(v: View?): Boolean {
                listener.onWarehouseLongClick(adapterPosition)
                return true
            }
        })
    }
}
