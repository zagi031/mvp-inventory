package com.example.zagorscak.mvpinventory.presenters

import android.net.ConnectivityManager
import android.util.Patterns
import com.example.zagorscak.mvpinventory.contracts.ContractSignUp
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest

class SignUpPresenter(private val viewSignUp: ContractSignUp.ViewContract) : ContractSignUp.PresenterContract {
    private val PASSWORD_MIN_LENGTH = 8
    private val mAuth = FirebaseAuth.getInstance()

    override fun getCurrentUser() = FirebaseAuth.getInstance().currentUser

    override fun checkConnectivity(cm: ConnectivityManager) {
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        if(isConnected){
            viewSignUp.onConnected()
        }
        else viewSignUp.onNotConnected()
    }

    override fun checkInput(email: String, password: String) {
        viewSignUp.setInputErrors(checkEmailInput(email), checkPasswordInput(password))
        if(checkEmailInput(email).isEmpty() && checkPasswordInput(password).isEmpty()){
            viewSignUp.onCorrectInput(email,password)
        }
    }

    override fun signUp(email: String, password: String) {
        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task: Task<AuthResult> ->
                if (task.isSuccessful) {
                    val name = viewSignUp.getName().trim()
                    val surname = viewSignUp.getSurname().trim()
                    val profileUpdate = UserProfileChangeRequest.Builder().setDisplayName(name.trim().capitalize()+" "+surname.trim().capitalize()).build()
                    mAuth.currentUser?.updateProfile(profileUpdate)?.addOnCompleteListener{ task ->
                        viewSignUp.onSuccessfulSignUp()
                    }
                } else {
                    viewSignUp.onUnsuccessfulSignUp()
                }
            }

    }

    private fun checkEmailInput(email: String): String {
        if (email.isEmpty()) {
            return "Field empty"
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return "Email not in right form"
        }
        return String()
    }

    private fun checkPasswordInput(password: String): String {
        if (password.isEmpty()) {
            return "Field empty"
        }
        if (password.length < PASSWORD_MIN_LENGTH) {
            return "Password too short"
        }
        return String()
    }
}