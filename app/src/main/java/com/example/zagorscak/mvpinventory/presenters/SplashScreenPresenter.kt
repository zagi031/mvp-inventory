package com.example.zagorscak.mvpinventory.presenters

import com.example.zagorscak.mvpinventory.contracts.ContractSplashScreen
import com.google.firebase.auth.FirebaseAuth

class SplashScreenPresenter(private val viewSplashScreen: ContractSplashScreen.ViewContract): ContractSplashScreen.PresenterContract {

    override fun checkUserLogin() {
        if(FirebaseAuth.getInstance().currentUser == null){
            viewSplashScreen.onUserNotLoggedIn()
        }
        else {
            viewSplashScreen.onUserLoggedIn()
        }
    }
}