package com.example.zagorscak.mvpinventory.presenters

import android.net.ConnectivityManager
import com.example.zagorscak.mvpinventory.loggers.FirebaseLogger
import com.example.zagorscak.mvpinventory.contracts.ContractWarehousesFragment
import com.example.zagorscak.mvpinventory.models.LogData
import com.example.zagorscak.mvpinventory.models.Warehouse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*

class WarehousesFragmentPresenter(private var viewWarehouses: ContractWarehousesFragment.ViewContract?) :
    ContractWarehousesFragment.PresenterContract  {
    private val mDatabase = FirebaseDatabase.getInstance()
    private val mDbRef = mDatabase.reference.child("warehouses")

    override fun checkConnectivity(cm: ConnectivityManager) {
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        if (isConnected) {
            viewWarehouses?.onConnected()
        } else viewWarehouses?.onNotConnected()
    }

    init {
        mDbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                viewWarehouses?.showToast(p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                val warehouses = mutableListOf<Warehouse>()
                p0.children.forEach {
                    val warehouse= it.getValue(Warehouse::class.java)!!
                    warehouses.add(warehouse)
                }
                viewWarehouses?.refreshWarehouses(warehouses)
            }
        })
    }

    override fun setView(view: ContractWarehousesFragment.ViewContract?) {
        this.viewWarehouses = view
    }

    override fun getWarehouseCurrentCapacity(warehouse: Warehouse): Int {
        var currentCapacity = 0
        warehouse.items.forEach {
            currentCapacity += it.value.quantity
        }
        return currentCapacity
    }

    private fun log(warehouse: Warehouse, action: String){
        val logData = LogData()
        logData.user = FirebaseAuth.getInstance().currentUser?.email.toString()
        val sdf = SimpleDateFormat("dd/MM/yyyy 'at' HH:mm")
        logData.time = sdf.format(Date())
        logData.action = action
        FirebaseLogger.Log(logData)
    }

    override fun addWarehouse(warehouse: Warehouse) {
        mDbRef.child(warehouse.id).setValue(warehouse)
        this.log(warehouse,"created new warehouse: ${warehouse.name}")
    }

    override fun deleteWarehouse(warehouse: Warehouse) {
        mDbRef.child(warehouse.id).removeValue()
        this.log(warehouse,"deleted warehouse: ${warehouse.name}")
    }

    override fun editWarehouse(warehouse: Warehouse) {
        mDbRef.child(warehouse.id).setValue(warehouse)
        this.log(warehouse,"edited warehouse info on warehouse: ${warehouse.name}")
    }
}