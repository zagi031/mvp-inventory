package com.example.zagorscak.mvpinventory.presenters

import android.annotation.SuppressLint
import android.net.ConnectivityManager
import com.example.zagorscak.mvpinventory.loggers.FirebaseLogger
import com.example.zagorscak.mvpinventory.contracts.ContractItemsFragment
import com.example.zagorscak.mvpinventory.models.Item
import com.example.zagorscak.mvpinventory.models.LogData
import com.example.zagorscak.mvpinventory.models.Warehouse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*

class ItemsFragmentPresenter(private var viewItems:ContractItemsFragment.ViewContract?,private var warehouseID: String) : ContractItemsFragment.PresenterContract {
    private val mDatabase = FirebaseDatabase.getInstance()
    private val mDbRef = mDatabase.reference.child("warehouses").child(warehouseID)
    private lateinit var warehouse:Warehouse

    override fun checkConnectivity(cm: ConnectivityManager) {
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        if (isConnected) {
            viewItems?.onConnected()
        } else viewItems?.onNotConnected()
    }

    init {
        mDbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                viewItems?.showToast(p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                val items = mutableListOf<Item>()
                if(p0.getValue(Warehouse::class.java) == null){
                    viewItems?.changeToWarehousesFragment()
                }
                else{
                    warehouse = p0.getValue(Warehouse::class.java)!!
                    p0.child("items").children.forEach {
                        val item = Item()
                        item.id = it.key.toString()
                        item.name = it.getValue(Item::class.java)?.name.toString()
                        item.quantity = it.getValue(Item::class.java)?.quantity!!.toInt()
                        items.add(item)
                    }
                    viewItems?.refreshItems(items)
                }
            }
        })
    }

    override fun setView(view: ContractItemsFragment.ViewContract?) {
        this.viewItems = view
    }

    override fun checkCapacity(item: Item) {
        val warehouseMaxCapacity = this.warehouse.maxCapacity
        if(warehouse.getCurrentCapacity() + item.quantity <= warehouseMaxCapacity){
            viewItems?.onEnoughSpace(item)
        }
        else{
            viewItems?.onNotEnoughSpace("Not enough space ${warehouse.getCurrentCapacity()}/${warehouseMaxCapacity}")
        }
    }

    override fun getWarehouseID() = this.warehouseID

    @SuppressLint("SimpleDateFormat")
    private fun log(action: String){
        val logData = LogData()
        logData.user = FirebaseAuth.getInstance().currentUser?.email.toString()
        val sdf = SimpleDateFormat("dd/MM/yyyy 'at' HH:mm")
        logData.time = sdf.format(Date())
        logData.action = action
        FirebaseLogger.Log(logData)
    }

    override fun addItem(item: Item) {
        mDbRef.child("items").child(item.id).setValue(item)
        this.log("added ${item.quantity}x ${item.name} to ${warehouse.name}")
    }

    override fun deleteItem(item: Item) {
        mDbRef.child("items").child(item.id).removeValue()
        this.log("deleted all ${item.name} from ${warehouse.name}")
    }
}