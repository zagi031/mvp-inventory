package com.example.zagorscak.mvpinventory.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.example.zagorscak.mvpinventory.R
import com.example.zagorscak.mvpinventory.contracts.ContractSplashScreen
import com.example.zagorscak.mvpinventory.presenters.SplashScreenPresenter
import com.google.firebase.auth.FirebaseAuth

class SplashScreen : AppCompatActivity(), ContractSplashScreen.ViewContract {
    private val presenter:ContractSplashScreen.PresenterContract = SplashScreenPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)

        Handler().postDelayed({
            presenter.checkUserLogin()
        }, 1500)
    }


    override fun onBackPressed() {}
    override fun onUserLoggedIn() {
        startActivity(Intent(this, MainActivity::class.java))
        Toast.makeText(this, "Welcome " + (FirebaseAuth.getInstance().currentUser?.displayName), Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun onUserNotLoggedIn() {
        startActivity(Intent(this, SignInActivity::class.java))
        finish()
    }


}
