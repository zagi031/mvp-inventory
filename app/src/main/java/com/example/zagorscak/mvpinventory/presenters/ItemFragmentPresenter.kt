package com.example.zagorscak.mvpinventory.presenters

import android.annotation.SuppressLint
import com.example.zagorscak.mvpinventory.loggers.FirebaseLogger
import com.example.zagorscak.mvpinventory.contracts.ContractItemFragment
import com.example.zagorscak.mvpinventory.models.Item
import com.example.zagorscak.mvpinventory.models.LogData
import com.example.zagorscak.mvpinventory.models.Warehouse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*


class ItemFragmentPresenter(
    private var viewItem: ContractItemFragment.ViewContract?,
    private var warehouseID: String,
    private var itemID: String
) : ContractItemFragment.PresenterContract {
    private lateinit var warehouse: Warehouse
    private lateinit var item: Item
    private val mDatabase = FirebaseDatabase.getInstance()
    private val mDbRef = mDatabase.reference.child("warehouses")
        .child(warehouseID)

    init {
        mDbRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                viewItem?.showToast(p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.getValue(Warehouse::class.java) == null || p0.child("items").child(itemID)
                        .getValue(Item::class.java) == null
                ) {
                    viewItem?.changeToWarehousesFragment()
                } else {
                    warehouse = p0.getValue(Warehouse::class.java)!!
                    item = p0.child("items").child(itemID).getValue(Item::class.java)!!
                    viewItem?.setUI()
                }
            }
        })
        mDatabase.reference.child("warehouses").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                viewItem?.showToast(p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                val warehouses = mutableListOf<Warehouse>()
                p0.children.forEach {
                    val warehouse = it.getValue(Warehouse::class.java)
                    if (warehouse != null && warehouse.id != warehouseID) {
                        warehouses.add(warehouse)
                    }
                }
                viewItem?.setSpinnerData(warehouses)
            }
        })
    }

    override fun getItemName() = item.name
    override fun getItemQuantity() = item.quantity
    override fun getMaxItemQuantity(): Int {
        var warehouseOtherItemsQuantity = 0
        warehouse.items.values.forEach {
            if (it.id != item.id) {
                warehouseOtherItemsQuantity += it.quantity
            }
        }
        return warehouse.maxCapacity - warehouseOtherItemsQuantity
    }

    override fun setNewQuantity(newQuantity: Int) {
        if (getMaxItemQuantity() >= newQuantity) {
            mDbRef.child("items").child(itemID).child("quantity").setValue(newQuantity)
            this.log("set new quantity for ${item.name} from ${item.quantity} to $newQuantity in ${warehouse.name}")
        } else viewItem?.showToast("Not enough space for selected quantity")
    }

    override fun renameItem(newName: String) {
        mDbRef.child("items").child(itemID).child("name").setValue(newName)
        this.log("renamed ${item.name} to $newName in ${warehouse.name}")
    }

    override fun moveItem(quantity: Int, destinationWarehouse: Warehouse) {
        val destinationWarehouseFreeSpace =
            destinationWarehouse.maxCapacity - destinationWarehouse.getCurrentCapacity()

        if (destinationWarehouseFreeSpace >= quantity) {
            val currentItemQuantity = this.warehouse.items.get(itemID)?.quantity
            if (currentItemQuantity != null) {
                mDatabase.reference.child("warehouses").child(warehouseID)
                    .child("items")
                    .child(itemID)
                    .child("quantity").setValue(currentItemQuantity - quantity)
            }
            if (currentItemQuantity == quantity) {
                deleteItem()
            }
            var destinationWarehouseContainsThatItem = false
            var destinationItemID = System.currentTimeMillis().toString()
            var destinationItemQuantity = 0
            destinationWarehouse.items.values.forEach {
                if (it.name.equals(item.name)) {
                    destinationItemID = it.id
                    destinationItemQuantity = it.quantity
                    destinationWarehouseContainsThatItem = true
                }
            }

            if (destinationWarehouseContainsThatItem) {
                val ref = mDatabase.reference.child("warehouses").child(destinationWarehouse.id)
                    .child("items").child(destinationItemID)
                ref.child("id").setValue("${destinationItemID}")
                ref.child("name").setValue(item.name)
                ref.child("quantity").setValue(destinationItemQuantity + quantity)
            } else {
                val ref =
                    mDatabase.reference.child("warehouses").child(destinationWarehouse.id)
                        .child("items").child(destinationItemID)
                ref.child("id").setValue("${destinationItemID}")
                ref.child("name").setValue(item.name)
                ref.child("quantity").setValue(quantity)
            }
            this.log("moved ${quantity}x ${item.name} from ${this.warehouse.name} to ${destinationWarehouse.name}")
        } else viewItem?.showToast("Not enough space in that warehouse ${destinationWarehouse.getCurrentCapacity()}/${destinationWarehouse.maxCapacity}")
    }

    override fun deleteItem() {
        mDatabase.reference.child("warehouses").child(warehouse.id).child("items")
            .child(item.id).removeValue()
        this.log("deleted all ${item.name} from ${this.warehouse.name}")
    }

    @SuppressLint("SimpleDateFormat")
    private fun log(action: String) {
        val logData = LogData()
        logData.user = FirebaseAuth.getInstance().currentUser?.email.toString()
        val sdf = SimpleDateFormat("dd/MM/yyyy 'at' HH:mm")
        logData.time = sdf.format(Date())
        logData.action = action
        FirebaseLogger.Log(logData)
    }

    override fun setView(view: ContractItemFragment.ViewContract?) {
        this.viewItem = view
    }
}
