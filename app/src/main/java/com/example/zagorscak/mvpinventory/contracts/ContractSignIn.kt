package com.example.zagorscak.mvpinventory.contracts

import android.net.ConnectivityManager
import com.google.firebase.auth.FirebaseUser

interface ContractSignIn {
    interface ViewContract {
        fun onConnected()
        fun onNotConnected()
        fun setInputErrors(emailError:String, passwordError:String)
        fun onCorrectInput(email: String, password: String)
        fun onSuccessfulSignIn()
        fun onUnsuccessfulSignIn()
    }

    interface PresenterContract {
        fun getCurrentUser(): FirebaseUser?
        fun checkConnectivity(cm:ConnectivityManager)
        fun checkInput(email: String, password: String)
        fun signIn(email:String, password:String)
    }
}