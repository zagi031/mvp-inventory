package com.example.zagorscak.mvpinventory.listenerInterfaces

interface onItemClickListener {
    fun onItemClick(position:Int)
}