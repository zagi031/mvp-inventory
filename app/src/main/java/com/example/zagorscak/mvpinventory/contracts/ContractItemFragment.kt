package com.example.zagorscak.mvpinventory.contracts

import com.example.zagorscak.mvpinventory.models.Warehouse

interface ContractItemFragment {
    interface ViewContract {
        fun setSpinnerData(warehouses: List<Warehouse>)
        fun showToast(message: String)
        fun setUI()
        fun changeToWarehousesFragment()
    }

    interface PresenterContract {
        fun getItemName(): String
        fun getItemQuantity(): Int
        fun setNewQuantity(newQuantity: Int)
        fun renameItem(newName: String)
        fun moveItem(quantity: Int, destinationWarehouse: Warehouse)
        fun setView(viewItem: ContractItemFragment.ViewContract?)
        fun getMaxItemQuantity(): Int
        fun deleteItem()
    }
}