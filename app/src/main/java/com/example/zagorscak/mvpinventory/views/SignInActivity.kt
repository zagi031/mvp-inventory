package com.example.zagorscak.mvpinventory.views

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.zagorscak.mvpinventory.R
import com.example.zagorscak.mvpinventory.contracts.ContractSignIn
import com.example.zagorscak.mvpinventory.presenters.SignInPresenter
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity(), ContractSignIn.ViewContract {
    private val presenter:ContractSignIn.PresenterContract = SignInPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btn_signIn_signIn.setOnClickListener {
            presenter.checkConnectivity(getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        }

        tv_signIn_signUp.setOnClickListener {
            val signUpIntent = Intent(this, SignUpActivity::class.java)
            startActivity(signUpIntent)
        }
    }

    override fun setInputErrors(emailError: String, passwordError: String) {
        til_signIn_email.error = emailError
        til_singIn_password.error = passwordError
    }

    override fun onConnected() {
        val email = et_signIn_email.text.toString()
        val password = et_signIn_password.text.toString()
        presenter.checkInput(email, password)
    }

    override fun onNotConnected() {
        Toast.makeText(this, "Check internet connection", Toast.LENGTH_LONG).show()
    }

    override fun onCorrectInput(email: String, password: String) {
        presenter.signIn(email, password)
    }

    override fun onSuccessfulSignIn() {
        val loggedInIntent = Intent(
            this,
            MainActivity::class.java
        ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(loggedInIntent)
        Toast.makeText(
            this,
            "Welcome " + (presenter.getCurrentUser()?.displayName),
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onUnsuccessfulSignIn() {
        Toast.makeText(this, "Incorrect email or password", Toast.LENGTH_SHORT).show()
    }
}
