package com.example.zagorscak.mvpinventory.contracts

import android.net.ConnectivityManager
import com.example.zagorscak.mvpinventory.models.Item
import com.example.zagorscak.mvpinventory.models.Warehouse

interface ContractItemsFragment {
    interface ViewContract{
        fun onConnected()
        fun onNotConnected()
        fun refreshItems(items:MutableList<Item>)
        fun showToast(text:String)
        fun onEnoughSpace(item: Item)
        fun onNotEnoughSpace(text: String)
        fun changeToWarehousesFragment()
    }

    interface PresenterContract{
        fun checkConnectivity(cm: ConnectivityManager)
        fun setView(view: ContractItemsFragment.ViewContract?)
        fun addItem(item: Item)
        fun deleteItem(item: Item)
        fun checkCapacity(item: Item)
        fun getWarehouseID():String
    }
}