package com.example.zagorscak.mvpinventory.contracts

import android.net.ConnectivityManager
import com.example.zagorscak.mvpinventory.models.Warehouse

interface ContractWarehousesFragment {
    interface ViewContract {
        fun onConnected()
        fun onNotConnected()
        fun refreshWarehouses(warehouses:MutableList<Warehouse>)
        fun showToast(text:String)
    }

    interface PresenterContract {
        fun checkConnectivity(cm: ConnectivityManager)
        fun setView(view: ViewContract?)
        fun addWarehouse(warehouse: Warehouse)
        fun deleteWarehouse(warehouse: Warehouse)
        fun editWarehouse(warehouse: Warehouse)
        fun getWarehouseCurrentCapacity(warehouse: Warehouse):Int
    }
}