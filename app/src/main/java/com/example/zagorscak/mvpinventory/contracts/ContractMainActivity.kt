package com.example.zagorscak.mvpinventory.contracts

import android.net.ConnectivityManager
import com.google.firebase.auth.FirebaseUser

interface ContractMainActivity {
    interface ViewContract {
        fun onSuccessfulSignOut()
        fun onUnSuccessfulSignOut()
    }

    interface PresenterContract {
        fun getCurrentUser(): FirebaseUser?
        fun signOut()
    }
}