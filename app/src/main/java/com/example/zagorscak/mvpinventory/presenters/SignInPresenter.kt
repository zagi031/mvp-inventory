package com.example.zagorscak.mvpinventory.presenters

import android.net.ConnectivityManager
import android.util.Patterns
import com.example.zagorscak.mvpinventory.contracts.ContractSignIn
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class SignInPresenter(private var viewSignIn: ContractSignIn.ViewContract) :
    ContractSignIn.PresenterContract {
    private val mAuth:FirebaseAuth = FirebaseAuth.getInstance()

    override fun getCurrentUser() = FirebaseAuth.getInstance().currentUser

    override fun checkConnectivity(cm:ConnectivityManager) {
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        if(isConnected){
            viewSignIn.onConnected()
        }
        else viewSignIn.onNotConnected()
    }

    override fun checkInput(email: String,password: String) {
        viewSignIn.setInputErrors(checkEmailInput(email),checkPasswordInput(password))
        if(checkEmailInput(email).isEmpty() && checkPasswordInput(password).isEmpty()){
            viewSignIn.onCorrectInput(email,password)
        }
    }

    override fun signIn(email: String, password: String) {
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener{task: Task<AuthResult> ->
            if(task.isSuccessful) {
                viewSignIn.onSuccessfulSignIn()
            }
            else{
                viewSignIn.onUnsuccessfulSignIn()
            }
        }
    }

    private fun checkEmailInput(email: String):String {
        if(email.isEmpty()) {
            return "Field empty"
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return "Email not in right form"
        }
        return String()
    }

    private fun checkPasswordInput(password: String):String {
        if(password.isEmpty())
        {
            return "Field empty"
        }
        return String()
    }
}